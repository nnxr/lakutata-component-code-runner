import {Application, Component, Configurable, Inject} from '@lakutata/core'
import {Compiler} from './lib/Compiler'
import {Runner} from './lib/Runner'
import os from 'os'
import {AnySchema} from 'yup'
import {isAsyncFunction} from 'util/types'
import {InvalidCRIFunctionException} from './exceptions/InvalidCRIFunctionException'
import {CompiledCodeObject} from './types/CompiledCodeObject'
import {SourceCodeObject} from './types/SourceCodeObject'
import {RunCodeResult} from './types/RunCodeResult'
import {ModuleFetcher} from './lib/ModuleFetcher'
import {RunCodeObject} from './types/RunCodeObject'
import {IRunCodeFileSystem} from './interfaces/IRunCodeFileSystem'
import {CodeRuntimeInterface} from './types/CodeRuntimeInterface'
import verdaccio from 'verdaccio'
import path from 'path'
import {CompilerNotReadyException} from './exceptions/CompilerNotReadyException'
import {WorkerStats} from './types/WorkerStats'

export {CompiledCodeObject} from './types/CompiledCodeObject'
export {SourceCodeObject} from './types/SourceCodeObject'
export {RunCodeObject} from './types/RunCodeObject'
export {RunCodeResult} from './types/RunCodeResult'
export {CodeRuntimeInterface} from './types/CodeRuntimeInterface'
export {WorkerStats} from './types/WorkerStats'
export {InvalidCRIFunctionException} from './exceptions/InvalidCRIFunctionException'
export {RuntimeCodeEnvironmentsException} from './exceptions/RuntimeCodeEnvironmentsException'
export {RuntimeCodeOutputException} from './exceptions/RuntimeCodeOutputException'
export {RuntimeCodeException} from './exceptions/RuntimeCodeException'
export {RuntimeCodeTimeoutException} from './exceptions/RuntimeCodeTimeoutException'
export {IRunCodeFileSystem} from './interfaces/IRunCodeFileSystem'


export class CodeRunnerComponent extends Component {

    @Inject(Application)
    protected readonly app: Application

    @Configurable()
    protected readonly logger: boolean = true

    @Configurable()
    protected readonly moduleRegistry: string | string[] = ['https://r.cnpmjs.org/', 'https://mirrors.cloud.tencent.com/npm/', 'https://registry.npmmirror.com/']

    @Configurable()
    protected readonly moduleRegistryTimeout: number = 20000

    @Configurable()
    protected readonly extractModulesPath: string

    @Configurable()
    protected readonly presetModules: string[]

    @Configurable()
    protected readonly tmpdir: string = os.tmpdir()

    @Configurable()
    protected readonly compilerOptions: { [key: string]: any }

    @Configurable()
    protected readonly fileSystem?: IRunCodeFileSystem

    @Configurable()
    protected readonly timeout: number = 60000

    @Configurable()
    protected readonly concurrent: number | { min: number; max: number }

    @Configurable()
    protected readonly outputSchema: AnySchema

    @Configurable()
    protected readonly CRI?: CodeRuntimeInterface

    protected fetcher: ModuleFetcher

    protected compiler: Compiler

    protected runner: Runner

    protected isCompilerReady: boolean = false

    /**
     * 编译器是否就绪
     * @constructor
     */
    public get COMPILER_READY(): boolean {
        return this.isCompilerReady
    }

    /**
     * 验证CRI接口方法是否符合要求
     * @protected
     */
    protected validateCRI(): void {
        if (this.CRI) {
            for (const functionName of Object.keys(this.CRI)) {
                if (!isAsyncFunction(this.CRI[functionName])) {
                    throw this.generateException(InvalidCRIFunctionException, 'Async function required')
                }
            }
        }
    }

    protected async initialize(): Promise<void> {
        this.validateCRI()
        const localModuleRegistry: string = await new Promise<string>(async (resolve, reject) => {
            const storage: string = path.resolve(this.extractModulesPath, './.registry-storage')
            const cache: string = path.resolve(this.extractModulesPath, './.registry-cache')
            const moduleRegistries: string[] = Array.isArray(this.moduleRegistry) ? this.moduleRegistry : [this.moduleRegistry]
            const uplinks: {
                [key: string]: {
                    url: string
                    timeout: string
                    strict_ssl: boolean
                    [key: string]: any
                }
            } = {}
            moduleRegistries.forEach((moduleRegistry: string, index: number) => {
                uplinks[`uplink_${index}`] = {
                    url: moduleRegistry,
                    timeout: `${this.moduleRegistryTimeout}ms`,
                    strict_ssl: false,
                    cache: true
                }
            })
            const proxy: string = Object.keys(uplinks).join(' ')
            verdaccio({
                storage: storage,
                web: {
                    enable: false,
                    title: this.app.getID(),
                    gravatar: false
                },
                uplinks: uplinks,
                logs: {level: 'error'},
                packages: {
                    '@*/*': {
                        access: '$all',
                        publish: '$all',
                        unpublish: '$all',
                        proxy: proxy
                    },
                    '**': {
                        access: '$all',
                        publish: '$all',
                        unpublish: '$all',
                        proxy: proxy
                    }
                }
            }, '127.0.0.1:4873', cache, '1.0.0', this.app.getID(), (webServer, address) => {
                try {
                    webServer.listen(address.port || address.path, address.host, () => {
                        return resolve(`${address.proto}://${address.host}:${address.port}`)
                    })
                } catch (error) {
                    return reject(error)
                }
            })
        })
        this.fetcher = new ModuleFetcher({
            app: this.app,
            logger: this.logger,
            moduleRegistry: localModuleRegistry,
            extractModulesPath: this.extractModulesPath,
            presetModules: this.presetModules ? this.presetModules : []
        })
        this.compiler = new Compiler({
            app: this.app,
            fetcher: this.fetcher,
            tmpdir: this.tmpdir,
            compilerOptions: this.compilerOptions ? this.compilerOptions : {}
        })
        const runnerConcurrentSetting: { min: number; max: number } = (() => {
            const defaultSetting = {
                min: Math.ceil(os.cpus().length / 2),
                max: os.cpus().length
            }
            if (this.concurrent) {
                if (typeof (this.concurrent) === 'object') {
                    if (this.concurrent.min && this.concurrent.max) {
                        const min: number = parseInt(this.concurrent.min.toString())
                        const max: number = parseInt(this.concurrent.max.toString())
                        if (min && max) {
                            return {
                                min: min,
                                max: max
                            }
                        }
                    }
                    return defaultSetting
                } else {
                    const numConcurrent: number = parseInt(this.concurrent.toString())
                    if (numConcurrent) {
                        return {
                            min: numConcurrent,
                            max: numConcurrent
                        }
                    } else {
                        return defaultSetting
                    }
                }
            } else {
                return defaultSetting
            }
        })()
        const defaultFileSystem: IRunCodeFileSystem = {
            read: async () => {
                return undefined
            },
            write: async () => {
                return
            },
            sizeLimit: '1MB'
        }
        let runnerFileSystem: IRunCodeFileSystem
        if (!this.fileSystem) {
            runnerFileSystem = defaultFileSystem
        } else {
            runnerFileSystem = Object.assign(defaultFileSystem, this.fileSystem)
        }
        this.runner = new Runner({
            app: this.app,
            codeRunner: this,
            codeRuntimeInterface: this.CRI ? this.CRI : (runCodeId: string, app: Application) => {
                return {}
            },
            runnerConcurrentSetting: runnerConcurrentSetting,
            fileSystem: runnerFileSystem,
            timeout: this.timeout,
            schema: this.outputSchema
        })
        await this.runner.initializeRPC()
        process.nextTick(() => {
            this.fetcher.initializePresetModules().then(() => {
                this.isCompilerReady = true
                if (this.logger) this.app.Logger.info('Compiler initialize success')
            }).catch(initializePresetModulesError => this.app.Logger.warning('Initialize preset modules failed:', initializePresetModulesError.message))
        })
    }

    /**
     * 获取工作进程池状态
     */
    public workerStats(): WorkerStats {
        return this.runner.workerStats()
    }

    /**
     * 从源码进行编译
     * @param sourceCodeObject
     */
    public async compile(sourceCodeObject: SourceCodeObject): Promise<CompiledCodeObject> {
        if (!this.isCompilerReady) throw this.generateException(CompilerNotReadyException, 'Compiler not ready')
        return await this.compiler.compile(sourceCodeObject)
    }

    /**
     * 执行编译后的代码
     */
    public async run(runCodeObject: RunCodeObject, environments: { [key: string]: any } = {}): Promise<RunCodeResult> {
        return await this.runner.run(runCodeObject, environments)
    }

    /**
     * 编译并运行源码
     * @param sourceCodeObject
     * @param environments
     */
    public async compileAndRun(sourceCodeObject: SourceCodeObject, environments: {
        [key: string]: any
    } = {}): Promise<RunCodeResult> {
        if (!this.isCompilerReady) throw this.generateException(CompilerNotReadyException, 'Compiler not ready')
        const compiledCodeObject: CompiledCodeObject = await this.compile(sourceCodeObject)
        return await this.run(Object.assign(compiledCodeObject, {
            id: this.app.Security.MD5(`_$GRCID::${this.app.Security.generateRandomString(32)}&TIMESTAMP::${Date.now()}`)
        }), environments)
    }
}

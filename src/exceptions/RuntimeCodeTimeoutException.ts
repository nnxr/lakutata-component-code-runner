import {Exception} from '@lakutata/core'

export class RuntimeCodeTimeoutException extends Exception{
    district: string
    errno: number | string='E_RUNTIME_CODE_TIMEOUT'
}

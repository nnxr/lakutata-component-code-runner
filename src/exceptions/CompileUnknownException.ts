import {Exception} from '@lakutata/core'

export class CompileUnknownException extends Exception {
    district: string
    errno: number | string = 'E_COMPILE_UNKNOWN'
}

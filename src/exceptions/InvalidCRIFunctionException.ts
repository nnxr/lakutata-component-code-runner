import {Exception} from '@lakutata/core'

export class InvalidCRIFunctionException extends Exception {
    district: string
    errno: number | string = 'E_INVALID_CRI_FUNCTION'
}

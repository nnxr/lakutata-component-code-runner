import {Exception} from '@lakutata/core'

export class CompileException extends Exception {
    district: string
    errno: number | string = 'E_COMPILE'
}

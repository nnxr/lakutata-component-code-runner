import {Exception} from '@lakutata/core'

export class RuntimeCodeOutputException extends Exception {
    district: string
    errno: number | string = 'E_RUNTIME_CODE_OUTPUT'
}

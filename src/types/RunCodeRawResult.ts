import {RunCodeResultLog} from './RunCodeResultLog'

export type RunCodeRawResult = {
    result: any
    errorMessage: string
    logs: RunCodeResultLog[]
    fileSystemBinary: number[]
}

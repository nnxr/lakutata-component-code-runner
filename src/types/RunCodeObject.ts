import {CompiledCodeObject} from './CompiledCodeObject'

export type RunCodeObject = {
    id: string
} & CompiledCodeObject

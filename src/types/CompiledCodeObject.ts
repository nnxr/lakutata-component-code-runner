import {SchemaObject} from 'ajv'

export type CompiledCodeObject = {
    environments: SchemaObject
    compiled: number[]
    schema: SchemaObject
}

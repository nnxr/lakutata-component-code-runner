import {SchemaObject} from 'ajv'

export type SourceCodeObject = {
    environments: SchemaObject
    source: string
    schema: SchemaObject
}

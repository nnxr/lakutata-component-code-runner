export type WorkerStats = {
    total: number
    busy: number
    idle: number
}

import {Application, Exception} from '@lakutata/core'
import {AnySchema} from 'yup'
import {CodeRunnerComponent, WorkerStats} from '../CodeRunnerComponent'
import {RuntimeCodeException} from '../exceptions/RuntimeCodeException'
import {RuntimeCodeOutputException} from '../exceptions/RuntimeCodeOutputException'
import {RuntimeCodeEnvironmentsException} from '../exceptions/RuntimeCodeEnvironmentsException'
import Ajv, {SchemaObject} from 'ajv'
import {RunCodeResult} from '../types/RunCodeResult'
import {RuntimeCodeTimeoutException} from '../exceptions/RuntimeCodeTimeoutException'
import path from 'path'
import {RunCodeResultLog} from '../types/RunCodeResultLog'
import {RunCodeObject} from '../types/RunCodeObject'
import {RunCodeRawResult} from '../types/RunCodeRawResult'
import {IRunCodeFileSystem} from '../interfaces/IRunCodeFileSystem'
import {parse as bytesParse} from 'bytes'
import {CodeRuntimeInterface} from '../types/CodeRuntimeInterface'
import Piscina from 'piscina'
import os from 'os'
import {Server as SocketServer} from 'socket.io'
import {createServer, Server as HttpServer} from 'http'
import {getPortPromise} from 'portfinder'

export class Runner {
    protected readonly app: Application

    protected readonly codeRunner: CodeRunnerComponent

    protected readonly codeRuntimeInterface: CodeRuntimeInterface

    protected readonly timeout: number

    protected readonly pool: Piscina

    protected readonly fileSystem: IRunCodeFileSystem

    protected readonly outputSchema?: AnySchema

    protected rpcAddress: string

    protected rpcPath: string = '/LCCR_RPC'

    protected runningTaskCount: number = 0

    constructor(options: {
        app: Application
        codeRunner: CodeRunnerComponent
        codeRuntimeInterface: CodeRuntimeInterface
        runnerConcurrentSetting: { min: number; max: number }
        fileSystem: IRunCodeFileSystem
        timeout: number
        schema?: AnySchema
    }) {
        this.app = options.app
        this.codeRunner = options.codeRunner
        this.codeRuntimeInterface = options.codeRuntimeInterface
        this.fileSystem = options.fileSystem
        this.timeout = options.timeout
        this.outputSchema = options.schema
        this.pool = new Piscina({
            filename: path.resolve(__dirname, './runtime/VirtualMachineThreadContainer.js'),
            concurrentTasksPerWorker: os.cpus().length,
            minThreads: options.runnerConcurrentSetting.min,
            maxThreads: options.runnerConcurrentSetting.max
        })
            .on('error', (err: Error) => this.app.Logger.error('Worker pool error:', err.message))
    }

    /**
     * 验证代码运行输出
     * @param codeOutput
     * @protected
     */
    protected async validateCodeOutput(codeOutput: any): Promise<any> {
        if (this.outputSchema) {
            return await this.outputSchema.validate(codeOutput, {strict: true, stripUnknown: false})
        } else {
            return codeOutput
        }
    }

    /**
     * 验证环境变量数据
     * @param schema
     * @param environments
     * @protected
     */
    protected async validateEnvironments(schema: SchemaObject, environments: { [key: string]: any }): Promise<{
        [key: string]: any
    }> {
        if (!schema) {
            return environments
        }
        const validator = new Ajv({
            allErrors: false,
            coerceTypes: true
        }).compile(schema)
        const valid = validator(environments)
        if (valid) {
            return environments
        } else {
            const error = validator.errors?.at(0)
            if (!error) {
                throw this.codeRunner.generateException(RuntimeCodeEnvironmentsException, 'Invalid environments')
            } else {
                throw this.codeRunner.generateException(RuntimeCodeEnvironmentsException, `${error.instancePath} ${error.message}`)
            }
        }
    }

    /**
     * 获取工作状态
     */
    public workerStats(): WorkerStats {
        const total: number = this.pool.options.maxThreads * this.pool.options.concurrentTasksPerWorker
        const idle: number = total - this.runningTaskCount
        return {
            total: total,
            busy: this.runningTaskCount,
            idle: idle > 0 ? idle : 0
        }
    }

    /**
     * 初始化RPC
     */
    public async initializeRPC(): Promise<void> {
        const rpcPort: number = await getPortPromise({startPort: 7000})
        const rpcAddressObject: URL = new URL('http://hostname')
        rpcAddressObject.hostname = '127.0.0.1'
        rpcAddressObject.port = rpcPort.toString()
        this.rpcAddress = rpcAddressObject.toString()
        return new Promise((resolve, reject) => {
            try {
                const httpServer: HttpServer = createServer()
                const rpcServer: SocketServer = new SocketServer(httpServer, {path: this.rpcPath})
                rpcServer.on('connect', client => {
                    client.on('invoke', (data, fn) => {
                        const invokeObject: {
                            $rci: string
                            name: string
                            args: any[]
                        } = {
                            $rci: data.$rci,
                            name: data.name,
                            args: data.args
                        }
                        const runCodeId: string = invokeObject.$rci
                        const rpcResponse = (error: Error | null, result: any) => fn({
                            error: error,
                            result: JSON.parse(JSON.stringify({result: result})).result
                        })
                        try {
                            const CRI = this.codeRuntimeInterface(runCodeId, this.app)
                            const CRI_FUNC = CRI[invokeObject.name]
                            if (CRI_FUNC) {
                                CRI_FUNC(...invokeObject.args).then(result => {
                                    rpcResponse(null, result)
                                }).catch(error => {
                                    rpcResponse(error, null)
                                })
                            } else {
                                rpcResponse(new Error(`${invokeObject.name} is not a function`), null)
                            }
                        } catch (e) {
                            rpcResponse(e as Error, null)
                        }
                    })
                })
                httpServer.listen(rpcPort, rpcAddressObject.hostname, () => resolve())
            } catch (e) {
                return reject(e)
            }
        })
    }

    /**
     * 执行编译后的代码对象
     * @param runCodeObject
     * @param environments
     */
    public async run(runCodeObject: RunCodeObject, environments: { [key: string]: any } = {}): Promise<RunCodeResult> {
        this.runningTaskCount += 1
        try {
            const result: RunCodeResult = await this.runTask(runCodeObject, environments)
            this.runningTaskCount -= 1
            return result
        } catch (e) {
            this.runningTaskCount -= 1
            throw e
        }
    }

    /**
     * 执行编译后的代码对象任务
     * @param runCodeObject
     * @param environments
     */
    public async runTask(runCodeObject: RunCodeObject, environments: {
        [key: string]: any
    } = {}): Promise<RunCodeResult> {
        return new Promise((resolve) => {
            const runCodeResult: RunCodeResult = {
                result: null,
                exception: null,
                logs: []
            }
            const runResolve = (result: { result: any; errorMessage: string; logs: RunCodeResultLog[] }) => {
                runCodeResult.exception = null
                runCodeResult.result = result.result
                runCodeResult.logs = result.logs
                resolve(runCodeResult)
            }
            const runReject = (exception: Exception | Error, logs: RunCodeResultLog[] = []) => {
                runCodeResult.result = null
                runCodeResult.logs = logs
                let runCodeResultException: Exception
                if (exception['code'] === 'ERR_SCRIPT_EXECUTION_TIMEOUT') {
                    runCodeResultException = this.codeRunner.generateException(RuntimeCodeTimeoutException, 'Timeout')
                } else if (exception instanceof Exception) {
                    runCodeResultException = exception
                } else {
                    runCodeResultException = this.codeRunner.generateException(RuntimeCodeException, exception.message)
                }
                runCodeResult.exception = {
                    err: runCodeResultException.err,
                    errMsg: runCodeResultException.errMsg,
                    errno: runCodeResultException.errno
                }
                resolve(runCodeResult)
            }
            let fileSystemBinaryHash: string = ''
            this.validateEnvironments(runCodeObject.environments, environments).then(environments => {
                const fileSystemSizeLimit: number = bytesParse(this.fileSystem.sizeLimit)
                return this.fileSystem.read(runCodeObject.id).then(fileSystemBinary => {
                    const numArrayFileSystemBinary: number[] | undefined = fileSystemBinary ? Array.from(fileSystemBinary) : undefined
                    if (numArrayFileSystemBinary) {
                        fileSystemBinaryHash = this.app.Security.MD5(Buffer.from(numArrayFileSystemBinary).toString('base64'))
                    } else {
                        fileSystemBinaryHash = ''
                    }
                    return this.pool.run({
                        runCodeId: runCodeObject.id,
                        rpcAddress: this.rpcAddress,
                        rpcPath: this.rpcPath,
                        compiledBuffer: runCodeObject.compiled,
                        environments: environments,
                        timeout: this.timeout,
                        fileSystemBinary: numArrayFileSystemBinary,
                        fileSystemSizeLimit: fileSystemSizeLimit
                    })
                })
            }).then(async proxyResult => {
                try {
                    const result: RunCodeRawResult = await Promise.resolve(JSON.parse(proxyResult as any)) as RunCodeRawResult
                    const fileSystemBinaryBuffer: Buffer = Buffer.from(result.fileSystemBinary)
                    if (this.app.Security.MD5(fileSystemBinaryBuffer.toString('base64')) !== fileSystemBinaryHash) {
                        await this.fileSystem.write(runCodeObject.id, fileSystemBinaryBuffer)
                    }
                    if (!result.errorMessage) {
                        this.validateCodeOutput(result.result).then(codeRunResult => {
                            result.result = codeRunResult
                            runResolve({
                                result: codeRunResult,
                                errorMessage: result.errorMessage,
                                logs: result.logs
                            })
                        }).catch(validateError => {
                            runReject(this.codeRunner.generateException(RuntimeCodeOutputException, validateError.message), result.logs)
                        })
                    } else {
                        runReject(this.codeRunner.generateException(RuntimeCodeException, result.errorMessage), result.logs)
                    }
                } catch (e) {
                    runReject(this.codeRunner.generateException(RuntimeCodeOutputException, (e as Error).message), [])
                }
            })
                .catch(validateEnvironmentsError => runReject(validateEnvironmentsError, []))
        })
    }
}

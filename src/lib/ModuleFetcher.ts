import {extract, manifest} from 'pacote'
import fs from 'fs'
import path from 'path'
import {Application} from '@lakutata/core'

export class ModuleFetcher {
    protected readonly app: Application

    protected readonly logger: boolean

    protected readonly moduleRegistry: string

    protected readonly extractModulesPath: string

    protected readonly cachePath: string

    protected readonly presetModules: string[] = [
        'typescript@~4.7.4',
        'reflect-metadata@^0.1.13',
        '@types/node@^16.11.7',
        'eventemitter2@^6.4.5',
        'ajv@^8.11.0'
    ]

    constructor(options: {
        app: Application
        logger: boolean
        moduleRegistry: string
        extractModulesPath: string
        presetModules?: string[]
    }) {
        this.app = options.app
        this.logger = options.logger
        this.extractModulesPath = options.extractModulesPath
        if (!this.extractModulesPath) throw new Error('Extract modules path is required')
        this.cachePath = path.resolve(this.extractModulesPath, './.cache')
        this.presetModules.push(...(options.presetModules ? options.presetModules : []))
        this.moduleRegistry = options.moduleRegistry
        if (!fs.existsSync(this.extractModulesPath)) {
            fs.mkdirSync(this.extractModulesPath, {recursive: true})
            fs.mkdirSync(this.cachePath, {recursive: true})
        }
    }

    /**
     * 拉取Module
     * @param moduleName
     * @param target
     * @param keepModuleVersionedName
     */
    protected async fetchModule(moduleName: string, target: string = this.extractModulesPath, keepModuleVersionedName: boolean = false): Promise<void> {
        try {
            const moduleManifest = await manifest(moduleName, {registry: this.moduleRegistry})
            const dependencies: { [key: string]: string } | undefined = moduleManifest.dependencies
            const versionedDependencies: string[] = []
            if (dependencies && Object.keys(dependencies).length > 0) {
                Object.keys(dependencies).forEach(name => {
                    const version: string = dependencies[name]
                    versionedDependencies.push(`${name}@${version}`)
                })
                const fetchErroredModules = await this.fetchModules(versionedDependencies)
                if (fetchErroredModules.length) {
                    throw fetchErroredModules[0].error
                }
            }
            const modulePath: string = path.resolve(target, keepModuleVersionedName ? moduleName : moduleManifest.name)
            if (!fs.existsSync(modulePath)) {
                await extract(moduleName, modulePath, {
                    cache: this.cachePath,
                    preferOnline: true,
                    where: target,
                    registry: this.moduleRegistry,
                    strictSSL: false,
                    retry: {
                        fetchRetries: 3
                    }
                })
            }
            //需要判断解压后是否为空文件夹，是空文件夹的话则代表模块并未拉取成功
            if (!fs.readdirSync(modulePath).length) {
                fs.rmSync(modulePath, {force: true, recursive: true})
                throw new Error(`Module ${moduleName} got empty data`)
            }
            if (this.logger) this.app.Logger.info('Module', moduleName, 'fetch success')
        } catch (e) {
            throw new Error((e as Error).message)
        }
    }

    /**
     * 拉取多个Module
     * @param moduleNames
     * @param target
     * @param keepModuleVersionedName
     * @protected
     */
    protected async fetchModules(moduleNames: string[], target: string = this.extractModulesPath, keepModuleVersionedName: boolean = false): Promise<{ moduleName: string; error: Error }[]> {
        const fetchModulePromises: Promise<{ moduleName: string; error: Error } | null>[] = []
        moduleNames.forEach(moduleName => {
            fetchModulePromises.push(new Promise((resolve) => {
                this.fetchModule(moduleName, target, keepModuleVersionedName).then(() => {
                    return resolve(null)
                }).catch(err => {
                    return resolve({moduleName: moduleName, error: err})
                })
            }))
        })
        const results = await Promise.all(fetchModulePromises)
        return results.filter(result => {
            return result !== null
        }) as { moduleName: string; error: Error }[]
    }

    /**
     * 复制Modules至目标目录
     * @param target
     */
    public async copyModules(target: string): Promise<void> {
        return new Promise((resolve, reject) => {
            if (this.extractModulesPath) {
                fs.cp(this.extractModulesPath, target, {
                    recursive: true,
                    errorOnExist: false,
                    force: true,
                    preserveTimestamps: true
                }, copyError => {
                    if (copyError) {
                        reject(copyError)
                    } else {
                        resolve()
                    }
                })
            } else {
                reject(new Error('Node modules path not found'))
            }
        })
    }

    /**
     * @param moduleName
     * @param target
     * @param keepModuleVersionedName
     */
    public async fetch(moduleName: string, target: string, keepModuleVersionedName: boolean): Promise<void>
    /**
     * @param moduleNames
     * @param target
     * @param keepModuleVersionedName
     */
    public async fetch(moduleNames: string[], target: string, keepModuleVersionedName: boolean): Promise<void>
    public async fetch(inp: string | string[], target: string, keepModuleVersionedName: boolean): Promise<void> {
        let moduleNames: string[]
        if (Array.isArray(inp)) {
            moduleNames = inp
        } else {
            moduleNames = [inp]
        }
        await this.fetchModules(moduleNames, target, keepModuleVersionedName)
    }

    /**
     * 初始化预设Module列表
     */
    public async initializePresetModules(): Promise<void> {
        return new Promise((resolve, reject) => {
            this.fetchModules(this.presetModules).then(fetchErrorModules => {
                if (!fetchErrorModules.length) return resolve()
                const reloadFetchErroredPresetModulesTimer = setTimeout(() => {
                    if (fetchErrorModules.length) {
                        const reloadModuleNames: string[] = []
                        fetchErrorModules.forEach(fetchErrorModule => {
                            if (this.logger) this.app.Logger.warning('Load compiler internal module failed:', fetchErrorModule.error.message)
                            reloadModuleNames.push(fetchErrorModule.moduleName)
                        })
                        this.fetchModules(reloadModuleNames).then(reloadFetchErrorModules => {
                            if (reloadFetchErrorModules.length) {
                                fetchErrorModules = reloadFetchErrorModules
                                reloadFetchErroredPresetModulesTimer.refresh()
                            } else {
                                return resolve()
                            }
                        }).catch(fetchErroredModulesError => {
                            if (this.logger) this.app.Logger.warning('Reload errored modules error:', fetchErroredModulesError.message)
                            reloadFetchErroredPresetModulesTimer.refresh()
                        })
                    } else {
                        return resolve()
                    }
                }, 1000)
            }).catch(reject)
        })
    }
}

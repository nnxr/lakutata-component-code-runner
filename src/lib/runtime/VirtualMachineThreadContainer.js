const {io} = require('socket.io-client')
const {EventEmitter} = require('events')
const {EventEmitter2} = require('eventemitter2')
const {VM} = require('vm2')
const {Volume, createFsFromVolume} = require('memfs')
const path = require('path')
const zlib = require('zlib')
const util = require('util')
const stream = require('stream')
const json5 = require('json5')
const pTimeout = require('p-timeout')
//设定虚拟机容器进程名称
process.title = 'VirtualMachineContainer'

/**
 * 解码虚拟文件系统
 * @param fileSystemBinary
 * @param callback
 */
function decodeFileSystemBinary(fileSystemBinary, callback) {
    if (fileSystemBinary) {
        let fileSystemBinaryBuffer = Buffer.from(fileSystemBinary)
        zlib.brotliDecompress(fileSystemBinaryBuffer, (brotliDecompressDecodeError, brotliDecompressDecodeResult) => {
            if (brotliDecompressDecodeError) {
                //使用brotli解码失败时降级为gunzip
                zlib.gunzip(fileSystemBinaryBuffer, (gunzipDecodeError, gunzipDecodeResult) => {
                    if (gunzipDecodeError) {
                        callback({})
                    } else {
                        return callback(json5.parse(gunzipDecodeResult.toString()))
                    }
                })
            } else {
                return callback(json5.parse(brotliDecompressDecodeResult.toString()))
            }
        })
    } else {
        callback({})
    }
}

/**
 * 编码虚拟文件系统
 */
function encodeFileSystemBinary(originalFileSystemBinary, fileSystemSizeLimit, directoryJSON, callback) {
    zlib.brotliCompress(Buffer.from(json5.stringify(directoryJSON)), (encodeError, encodeResult) => {
        const _callback = function (fileSystemBinary) {
            let resolveFileSystemBinary
            if (fileSystemBinary.length >= fileSystemSizeLimit) {
                resolveFileSystemBinary = originalFileSystemBinary
            } else {
                resolveFileSystemBinary = fileSystemBinary
            }
            callback(resolveFileSystemBinary)
        }
        if (encodeError) {
            _callback(Array.from(zlib.brotliCompressSync(Buffer.from(json5.stringify({})))))
        } else {
            _callback(Array.from(encodeResult))
        }
    })
}

module.exports = async function (options) {
    let runCodeId = options.runCodeId
    let rpcAddress = options.rpcAddress
    let rpcPath = options.rpcPath
    let compiledBuffer = options.compiledBuffer
    let environments = options.environments
    let fileSystemBinary = options.fileSystemBinary
    let fileSystemSizeLimit = options.fileSystemSizeLimit
    let timeout = options.timeout
    return pTimeout(new Promise((resolve) => {
        decodeFileSystemBinary(fileSystemBinary, (directoryJSON) => {
            let originalFileSystemBinary = fileSystemBinary
            let dirname = '/'
            let filename = `${runCodeId}.vmc`
            let resultEventEmitter = null
            let codeRuntimeInterfaceEventEmitter = null
            let ipcConnection = null
            let ipcInvoke = null
            let logs = null
            let virtualMachineFileSystem = null
            let builtInModuleMap = null
            let immediateIdSet = null
            let intervalIdSet = null
            let timeoutIdSet = null
            let safeSetImmediate = null
            let safeSetInterval = null
            let safeSetTimeout = null
            let safeClearImmediate = null
            let safeClearInterval = null
            let safeClearTimeout = null
            let safeClearAllTimers = null
            let vmRequire = null
            let virtualMachine = null
            let compiledCode = null
            let runResolve = (result) => {
                socket.disconnect()
                socket.removeAllListeners()
                if (resultEventEmitter) resultEventEmitter.removeAllListeners()
                if (codeRuntimeInterfaceEventEmitter) codeRuntimeInterfaceEventEmitter.removeAllListeners()
                //清理对象及变量
                dirname = undefined
                filename = undefined
                resultEventEmitter = undefined
                codeRuntimeInterfaceEventEmitter = undefined
                runCodeId = undefined
                rpcAddress = undefined
                rpcPath = undefined
                socket = undefined
                compiledBuffer = undefined
                environments = undefined
                fileSystemBinary = undefined
                directoryJSON = undefined
                fileSystemSizeLimit = undefined
                timeout = undefined
                originalFileSystemBinary = undefined
                ipcConnection = undefined
                ipcInvoke = undefined
                logs = undefined
                virtualMachineFileSystem = undefined
                if (builtInModuleMap) builtInModuleMap.clear()
                builtInModuleMap = undefined
                immediateIdSet = undefined
                intervalIdSet = undefined
                timeoutIdSet = undefined
                safeSetImmediate = undefined
                safeSetInterval = undefined
                safeSetTimeout = undefined
                safeClearImmediate = undefined
                safeClearInterval = undefined
                safeClearTimeout = undefined
                safeClearAllTimers = undefined
                vmRequire = undefined
                virtualMachine = undefined
                compiledCode = undefined
                runResolve = undefined
                return resolve(JSON.stringify(result))
            }
            let socket = io(rpcAddress, {path: rpcPath})
            socket.on('connect', () => {
                ipcInvoke = (name, args, ack) => {
                    socket.emit('invoke', {
                        $rci: runCodeId,
                        name: name,
                        args: args
                    }, response => ack(response.error, response.result))
                }
                resultEventEmitter = new EventEmitter()
                codeRuntimeInterfaceEventEmitter = new EventEmitter2({
                    newListener: false,
                    removeListener: false,
                    wildcard: true,
                    ignoreErrors: true,
                    maxListeners: 16,
                    verboseMemoryLeak: true
                })
                //代码输出日志记录
                logs = []
                //虚拟机内自带模块
                virtualMachineFileSystem = Volume.fromJSON(directoryJSON, '/')
                builtInModuleMap = new Map([
                    ['fs', createFsFromVolume(virtualMachineFileSystem)],//FS模块
                    ['path', path],//Path模块
                    ['util', util],//Util模块
                    ['stream', stream]//Stream模块
                ])
                immediateIdSet = new Set()
                intervalIdSet = new Set()
                timeoutIdSet = new Set()
                safeSetImmediate = (...args) => {
                    const immediateId = setImmediate.apply(setImmediate, args)
                    immediateIdSet.add(immediateId)
                    return immediateId
                }
                safeSetInterval = (...args) => {
                    const intervalId = setInterval.apply(setInterval, args)
                    intervalIdSet.add(intervalId)
                    return intervalId
                }
                safeSetTimeout = (...args) => {
                    const timeoutId = setTimeout.apply(setTimeout, args)
                    timeoutIdSet.add(timeoutId)
                    return timeoutId
                }
                safeClearImmediate = (id) => {
                    clearImmediate(id)
                    immediateIdSet.delete(id)
                }
                safeClearInterval = (id) => {
                    clearInterval(id)
                    intervalIdSet.delete(id)
                }
                safeClearTimeout = (id) => {
                    clearTimeout(id)
                    timeoutIdSet.delete(id)
                }
                safeClearAllTimers = () => {
                    immediateIdSet.forEach(id => safeClearImmediate(id))
                    intervalIdSet.forEach(id => safeClearInterval(id))
                    timeoutIdSet.forEach(id => safeClearTimeout(id))
                }
                resultEventEmitter.on('result', result => {
                    encodeFileSystemBinary(originalFileSystemBinary, fileSystemSizeLimit, virtualMachineFileSystem.toJSON(), fileSystemBinary => {
                        safeClearAllTimers()
                        runResolve({
                            result: result,
                            errorMessage: null,
                            logs: logs,
                            fileSystemBinary: fileSystemBinary
                        })
                    })
                }).on('error', error => {
                    encodeFileSystemBinary(originalFileSystemBinary, fileSystemSizeLimit, virtualMachineFileSystem.toJSON(), fileSystemBinary => {
                        safeClearAllTimers()
                        runResolve({
                            result: null,
                            errorMessage: error.message,
                            logs: logs,
                            fileSystemBinary: fileSystemBinary
                        })
                    })
                })
                codeRuntimeInterfaceEventEmitter.onAny((event, strData) => {
                    return new Promise((resolve, reject) => {
                        const args = json5.parse(strData).args
                        ipcInvoke(event, args, (error, result) => {
                            if (error) {
                                reject(new Error(error.message))
                            } else {
                                resolve(result)
                            }
                        })
                    })
                })
                vmRequire = (moduleId) => {
                    if (builtInModuleMap.has(moduleId)) {
                        return builtInModuleMap.get(moduleId)
                    } else {
                        throw new Error(`Cannot find module '${moduleId}'`)
                    }
                }
                virtualMachine = new VM({
                    allowAsync: true,
                    eval: true,
                    wasm: false,
                    timeout: timeout,
                    sandbox: {
                        __dirname: dirname,
                        __filename: filename,
                        setImmediate: safeSetImmediate,
                        setInterval: safeSetInterval,
                        setTimeout: safeSetTimeout,
                        clearImmediate: safeClearImmediate,
                        clearInterval: safeClearInterval,
                        clearTimeout: safeClearTimeout,
                        module: {},
                        require: vmRequire,
                        process: {
                            env: {
                                PWD: '/',
                                HOME: '/',
                                USER: 'anonymous',
                                PATH: '/'
                            },
                            platform: 'vmc',
                            _$TIMEOUT: timeout,
                            _$REE: resultEventEmitter,
                            _$ENV: environments,
                            _$CRIEE: codeRuntimeInterfaceEventEmitter
                        },
                        console: {
                            log: (...args) => {
                                logs.push({
                                    level: 'log',
                                    args: args,
                                    timestamp: Date.now()
                                })
                            },
                            info: (...args) => {
                                logs.push({
                                    level: 'info',
                                    args: args,
                                    timestamp: Date.now()
                                })
                            },
                            error: (...args) => {
                                logs.push({
                                    level: 'error',
                                    args: args,
                                    timestamp: Date.now()
                                })
                            },
                            warn: (...args) => {
                                logs.push({
                                    level: 'warn',
                                    args: args,
                                    timestamp: Date.now()
                                })
                            }
                        }
                    }
                })
                let compressedCompiledBuffer = Buffer.from(compiledBuffer)
                let executeCompiledCode = (decompressedCompiledBuffer) => {
                    compiledCode = decompressedCompiledBuffer.toString()
                    try {
                        virtualMachine.run(compiledCode)
                    } catch (e) {
                        encodeFileSystemBinary(originalFileSystemBinary, fileSystemSizeLimit, virtualMachineFileSystem.toJSON(), fileSystemBinary => {
                            runResolve({
                                result: null,
                                errorMessage: e.message,
                                logs: logs,
                                fileSystemBinary: fileSystemBinary
                            })
                        })
                    }
                }
                zlib.brotliDecompress(compressedCompiledBuffer, (brotliDecompressError, brotliDecompressResult) => {
                    if (brotliDecompressError) {
                        zlib.gunzip(compressedCompiledBuffer, (gunzipError, gunzipResult) => {
                            compressedCompiledBuffer = undefined
                            if (gunzipError) {
                                runResolve({
                                    result: null,
                                    errorMessage: brotliDecompressError.message,
                                    logs: [],
                                    fileSystemBinary: fileSystemBinary
                                })
                            } else {
                                executeCompiledCode(gunzipResult)
                            }
                        })
                    } else {
                        compressedCompiledBuffer = undefined
                        executeCompiledCode(brotliDecompressResult)
                    }
                })
            })
        })
    }), timeout)
}

const nccCompile = require('@vercel/ncc')
process.title = 'NCC-Compiler'
process.on('message', data => {
    const projectEntryFilename = data.projectEntryFilename
    const options = data.options
    nccCompile(projectEntryFilename, options).then(({code}) => {
        process.send({
            event: 'result',
            data: code
        })
    }).catch(error => {
        process.send({
            event: 'result',
            data: error
        })
    })
})
process.send({
    event: 'ready'
})

import fs from 'fs'
import path from 'path'
import {ClassDeclaration, Project, SourceFile, StructureKind, VariableDeclarationKind} from 'ts-morph'
import {Application} from '@lakutata/core'
import {SourceCodeObject} from '../types/SourceCodeObject'
import {SchemaObject} from 'ajv'
import {CompiledCodeObject} from '../types/CompiledCodeObject'
import {ModuleFetcher} from './ModuleFetcher'
import detective from 'detective'
import typescript from 'typescript'
import moduleIdParser from 'module-id'
import * as zlib from 'zlib'
import {ChildProcess, fork} from 'child_process'
import {CompileUnknownException} from '../exceptions/CompileUnknownException'
import {CompileException} from '../exceptions/CompileException'
import os from 'os'

export class Compiler {
    protected readonly app: Application

    protected readonly internalModules: string[] = [
        'assert',
        'buffer',
        'child_process',
        'cluster',
        'crypto',
        'dgram',
        'dns',
        'domain',
        'events',
        'fs',
        'http',
        'https',
        'net',
        'os',
        'path',
        'punycode',
        'querystring',
        'readline',
        'stream',
        'string_decoder',
        'timers',
        'tls',
        'tty',
        'url',
        'util',
        'v8',
        'vm',
        'zlib'
    ]

    protected readonly entryFilename: string = 'index.ts'

    protected readonly tmpdir: string

    protected readonly fetcher: ModuleFetcher

    protected compilerOptions: { [key: string]: any } = {
        alwaysStrict: true,
        strict: true,
        module: 'CommonJS',
        target: 'ES2019',
        declaration: false,
        sourceMap: false,
        emitDecoratorMetadata: true,
        experimentalDecorators: true,
        downlevelIteration: true,
        removeComments: true,
        allowJs: true,
        checkJs: true,
        pretty: true,
        noUnusedLocals: false,
        noImplicitAny: false,
        noUnusedParameters: false,
        noImplicitReturns: true,
        moduleResolution: 'Node',
        esModuleInterop: true,
        strictPropertyInitialization: false,
        strictNullChecks: true,
        allowUnusedLabels: false,
        allowUnreachableCode: false,
        noImplicitThis: true,
        strictBindCallApply: true,
        strictFunctionTypes: true,
        allowSyntheticDefaultImports: false,
        types: [
            'reflect-metadata',
            'node'
        ],
        lib: [
            'ESNext',
            'ES2019'
        ]
    }

    constructor(options: {
        app: Application
        fetcher: ModuleFetcher,
        tmpdir: string
        compilerOptions: { [key: string]: any }
    }) {
        this.app = options.app
        this.fetcher = options.fetcher
        this.tmpdir = options.tmpdir
        if (!fs.existsSync(this.tmpdir)) {
            fs.mkdirSync(this.tmpdir, {recursive: true})
        }
        this.compilerOptions = Object.assign(this.compilerOptions, options.compilerOptions)
    }

    /**
     * 生成TsConfig文件
     * @param projectDir
     * @protected
     */
    protected generateTsConfigFile(projectDir: string): string {
        const tsConfigFilePath = path.resolve(projectDir, './tsconfig.json')
        fs.writeFileSync(tsConfigFilePath, JSON.stringify({
            compilerOptions: this.compilerOptions
        }, null, '  '))
        return tsConfigFilePath
    }

    /**
     * 生成编译项目源文件
     * @param project
     * @param projectPath
     * @param dependencyDir
     * @param entryFilename
     * @param sourceCode
     * @param outputSchema
     * @protected
     */
    protected generateSourceFiles(project: Project, projectPath: string, dependencyDir: string, entryFilename: string, sourceCode: string, outputSchema?: SchemaObject): void {
        const entrySourceFile: SourceFile = project.createSourceFile(entryFilename)
        entrySourceFile.addImportDeclarations([
            {
                moduleSpecifier: 'events',
                namedImports: [{name: 'EventEmitter'}]
            },
            {
                moduleSpecifier: path.resolve(dependencyDir, './BaseCodeClass'),
                namedImports: [{name: 'BaseCodeClass'}]
            },
            {
                moduleSpecifier: path.resolve(dependencyDir, './CodeOutputValidationDecorator'),
                namedImports: [{name: 'CodeOutputValidationDecorator'}]
            }
        ])
        const codeClass: ClassDeclaration = entrySourceFile.addClass({
            kind: StructureKind.Class,
            name: 'Code',
            extends: 'BaseCodeClass'
        })
        const mainMethod = codeClass.addMethod({
            kind: StructureKind.Method,
            name: 'main',
            parameters: [{
                name: 'CRI',
                type: '{ [key: string]: (...args: any[]) => Promise<any> }'
            }, {
                name: 'ENV',
                type: '{ [key: string]: any }'
            }],
            isAsync: true,
            statements: writer => {
                writer.writeLine(sourceCode)
            }
        })
        if (outputSchema && Object.keys(outputSchema).length > 0) {
            mainMethod.addDecorator({
                kind: StructureKind.Decorator,
                name: 'CodeOutputValidationDecorator',
                arguments: [JSON.stringify(outputSchema)]
            })
        }
        entrySourceFile.addStatements(writer => {
            writer.writeLine('new Code()')
        })
    }

    /**
     * 生成BaseCodeClass文件
     * @param project
     * @param dependencyDir
     * @protected
     */
    protected generateBaseCodeClass(project: Project, dependencyDir: string): void {
        const sourceFile = project.createSourceFile(path.resolve(dependencyDir, 'BaseCodeClass.ts'))
        sourceFile.addImportDeclarations([
            {
                moduleSpecifier: 'events',
                namedImports: [{name: 'EventEmitter'}]
            },
            {
                moduleSpecifier: 'eventemitter2',
                namedImports: [{name: 'EventEmitter2'}]
            }
        ])
        sourceFile.addClass({
            name: 'BaseCodeClass',
            isExported: true,
            properties: [
                {
                    name: 'ENV',
                    type: '{ [key: string]: any }',
                    isReadonly: true
                },
                {
                    name: 'CRI',
                    type: '{ [key: string]: (...args: any[]) => Promise<any> }',
                    isReadonly: true
                }
            ],
            methods: [
                {
                    name: 'constructor',
                    isAsync: false,
                    statements: writer => {
                        writer.writeLine(`const REE: EventEmitter = process['_$REE']`)
                        writer.writeLine(`const CRIEE: EventEmitter2 = process['_$CRIEE']`)
                        writer.writeLine(`const TIMEOUT: number = process['_$TIMEOUT']`)
                        writer.writeLine(`this.ENV = process['_$ENV']`)
                        writer.writeLine(`this.CRI = new Proxy({}, {`)
                        writer.writeLine(`get(target: {}, propertyKey: string): any {`)
                        writer.writeLine(`return new Proxy((...args: any[]) => {}, {`)
                        writer.writeLine(`apply(target: (...args: any[]) => void, thisArg: any, argArray: any[]): any {`)
                        writer.writeLine(`return new Promise((resolve, reject) => {`)
                        writer.writeLine(`const argumentObject = {args: argArray}`)
                        writer.writeLine(`CRIEE.emitAsync(propertyKey, JSON.stringify(argumentObject)).then(results => {resolve(results[0])}).catch(reject)`)
                        writer.writeLine(`})`)
                        writer.writeLine(`}`)
                        writer.writeLine(`})`)
                        writer.writeLine(`},`)
                        writer.writeLine(`set(target: {}, p: string | symbol, value: any, receiver: any): boolean {return true}`)
                        writer.writeLine(`})`)
                        writer.writeLine(`let finalized: boolean = false`)
                        writer.writeLine(`const resolve = (result: any) => {`)
                        writer.writeLine(`if (!finalized) {`)
                        writer.writeLine(`finalized = true`)
                        writer.writeLine(`REE.emit('result', result)`)
                        writer.writeLine(`}`)
                        writer.writeLine(`}`)
                        writer.writeLine(`const reject = (error: Error) => {`)
                        writer.writeLine(`if (!finalized) {`)
                        writer.writeLine(`finalized = true`)
                        writer.writeLine(`REE.emit('error', error)`)
                        writer.writeLine(`}`)
                        writer.writeLine(`}`)
                        writer.writeLine(`setTimeout(() => {return reject(new Error('Timeout'))}, TIMEOUT)`)
                        writer.writeLine(`this.main(this.CRI, this.ENV).then(result => {return resolve(result)}).catch(error => {return reject(error)})`)
                    }
                },
                {
                    name: 'main',
                    isAsync: true,
                    returnType: 'Promise<any>',
                    parameters: [{
                        name: 'CRI',
                        type: '{ [key: string]: (...args: any[]) => Promise<any> }'
                    }, {
                        name: 'ENV',
                        type: '{ [key: string]: any }'
                    }],
                    statements: writer => {
                        writer.writeLine(`throw new Error('Empty Code')`)
                    }
                }
            ]
        })
    }

    /**
     * 生成CodeOutputValidationDecorator文件
     * @param project
     * @param dependencyDir
     * @protected
     */
    protected generateCodeOutputValidationDecorator(project: Project, dependencyDir: string): void {
        const sourceFile = project.createSourceFile(path.resolve(dependencyDir, 'CodeOutputValidationDecorator.ts'))
        sourceFile.addImportDeclarations([
            {
                defaultImport: 'Ajv',
                moduleSpecifier: 'ajv'
            },
            {
                moduleSpecifier: 'ajv',
                namedImports: [
                    {name: 'Schema'},
                    {name: 'ValidateFunction'}
                ]
            }
        ])
        sourceFile.addVariableStatement({
            isExported: true,
            declarationKind: VariableDeclarationKind.Const,
            declarations: [{
                name: 'CodeOutputValidationDecorator',
                initializer: writer => {
                    writer.writeLine('(jsonSchema: Schema) => {')
                    writer.writeLine('return (target: Object, propertyKey: string, descriptor: TypedPropertyDescriptor<(...args: any[]) => any | Promise<any>>) => {')
                    writer.writeLine('descriptor.writable = false')
                    writer.writeLine('const validator: ValidateFunction = new Ajv({allErrors: false}).compile(jsonSchema)')
                    writer.writeLine('const originalMethod: (...args: any[]) => any | Promise<any> = descriptor.value as any')
                    writer.writeLine('const validate = (originalMethodResult) => {')
                    writer.writeLine('if (validator(originalMethodResult)) {')
                    writer.writeLine('return originalMethodResult')
                    writer.writeLine('} else {')
                    writer.writeLine('const errors = validator.errors')
                    writer.writeLine('if (errors && errors[0]) {')
                    writer.writeLine('const validateErrorObject = errors[0]')
                    writer.writeLine('let errorMessage: string = validateErrorObject.message ? validateErrorObject.message : \'validate failed\'')
                    writer.writeLine('if (validateErrorObject.instancePath) {')
                    writer.writeLine('let instancePath: string = validateErrorObject.instancePath.trim()')
                    writer.writeLine('instancePath = instancePath.replaceAll(/\\//g, \'.\')')
                    writer.writeLine('if (instancePath.indexOf(\'.\') === 0) {')
                    writer.writeLine('instancePath = instancePath.substring(1)')
                    writer.writeLine('}')
                    writer.writeLine('if (instancePath) {')
                    writer.writeLine('errorMessage = `${instancePath} ${errorMessage}`')
                    writer.writeLine('}')
                    writer.writeLine('}')
                    writer.writeLine('throw new Error(errorMessage)')
                    writer.writeLine('} else {')
                    writer.writeLine('throw new Error(\'Validate failed\')')
                    writer.writeLine('}')
                    writer.writeLine('}')
                    writer.writeLine('}')
                    writer.writeLine('descriptor.value = function (...args: any[]) {')
                    writer.writeLine('const originalMethodResult = originalMethod.apply(this, args)')
                    writer.writeLine('if (originalMethodResult instanceof Promise) {')
                    writer.writeLine('return new Promise((resolve, reject) => {')
                    writer.writeLine('return originalMethodResult.then(originalAsyncMethodResult => {')
                    writer.writeLine('try {')
                    writer.writeLine('resolve(validate(originalAsyncMethodResult))')
                    writer.writeLine('} catch (e) {')
                    writer.writeLine('reject(e)')
                    writer.writeLine('}')
                    writer.writeLine('}).catch(reject)')
                    writer.writeLine('})')
                    writer.writeLine('} else {')
                    writer.writeLine('return validate(originalMethodResult)')
                    writer.writeLine('}')
                    writer.writeLine('}')
                    writer.writeLine('return descriptor')
                    writer.writeLine('}')
                    writer.writeLine('}')
                }
            }]
        })
    }

    /**
     * 使用子进程进行NCC编译
     * @param projectEntryFilename
     * @param options
     * @protected
     */
    protected async nccProcessCompile(projectEntryFilename: string, options: { [key: string]: any }): Promise<string | Error> {
        return new Promise((resolve, reject) => {
            const nccProcess: ChildProcess = fork(path.resolve(__dirname, './runtime/NccCompiler.js'), {serialization: 'advanced'})
            nccProcess.on('message', (packet: { event: string; data?: any }) => {
                switch (packet.event) {
                    case 'ready': {
                        nccProcess.send({
                            projectEntryFilename: projectEntryFilename,
                            options: options
                        })
                    }
                        break
                    case 'result': {
                        resolve(packet.data)
                        nccProcess.kill()
                    }
                        break
                    case 'error': {
                        reject(packet.data)
                        nccProcess.kill()
                    }
                        break
                }
            })
        })
    }

    /**
     * 执行NCC编译
     * @protected
     * @param nccCompileTmpDir
     * @param sourceCodeObject
     */
    protected async nccCompile(nccCompileTmpDir: string, sourceCodeObject: SourceCodeObject): Promise<Buffer> {
        return new Promise<Buffer>((resolve, reject) => {
            const nccCompileResolve = (code: string) => {
                if (code.indexOf('__vmc_require__') !== -1) {
                    code = code.replace(/__vmc_require__/g, '__vmc_require2__')
                }
                code = code.replace(/__nccwpck_require__/g, '__vmc_require__')
                zlib.brotliCompress(Buffer.from(code), (error, buffer) => {
                    if (error) {
                        reject(error)
                    } else {
                        resolve(buffer)
                    }
                })
            }
            const nccCompileReject = (exception: any) => {
                reject(exception)
            }
            const dependencies: string[] = this.resolveCodeDependencies(sourceCodeObject.source)
            fs.mkdirSync(path.resolve(nccCompileTmpDir, './node_modules'))
            const nccCompileTmpDirNodeModules: string = path.resolve(nccCompileTmpDir, './node_modules')
            this.fetcher.copyModules(nccCompileTmpDirNodeModules).then(() => {
                const tsConfigFilePath: string = this.generateTsConfigFile(nccCompileTmpDir)
                const dependencyDir: string = path.resolve(nccCompileTmpDir, './deps')
                this.fetcher.fetch(dependencies, nccCompileTmpDirNodeModules, true).then(() => {
                    const project = new Project({
                        tsConfigFilePath: tsConfigFilePath
                    })
                    const projectEntryFilename: string = path.resolve(nccCompileTmpDir, `./${this.entryFilename}`)
                    this.generateBaseCodeClass(project, dependencyDir)
                    this.generateCodeOutputValidationDecorator(project, dependencyDir)
                    this.generateSourceFiles(project, nccCompileTmpDir, dependencyDir, projectEntryFilename, sourceCodeObject.source, sourceCodeObject.schema)
                    project.save().then(() => {
                        this.nccProcessCompile(projectEntryFilename, {
                            cache: false,
                            filterAssetBase: nccCompileTmpDir,
                            minify: true,
                            sourceMap: false,
                            assetBuilds: false,
                            sourceMapRegister: false,
                            watch: false,
                            v8cache: false,
                            quiet: true,
                            debugLog: false
                        }).then(codeOrErrorObject => {
                            if (typeof codeOrErrorObject === 'string') {
                                nccCompileResolve(codeOrErrorObject)
                            } else {
                                if (!codeOrErrorObject.message) {
                                    nccCompileReject(new CompileUnknownException('Unknown compile error'))
                                } else {
                                    const nonColorMessages: string[] = codeOrErrorObject.message.replace(/\u001b\[.*?m/g, '').split(os.EOL)
                                    let compileErrorMessage: string = 'Unknown compile error'
                                    for (const nonColorMessage of nonColorMessages) {
                                        if (!nonColorMessage.includes('[tsl] ERROR in')) {
                                            compileErrorMessage = nonColorMessage.trim()
                                            break
                                        }
                                    }
                                    nccCompileReject(new CompileException(compileErrorMessage))
                                }
                            }
                        }).catch(nccCompileReject)
                    }).catch(nccCompileReject)
                }).catch(nccCompileReject)
            }).catch(nccCompileReject)
        })
    }

    /**
     * 解析源码中的依赖包列表
     * @param sourceCode
     * @protected
     */
    public resolveCodeDependencies(sourceCode: string): string[] {
        const moduleNameRegExp: RegExp = /^(@[a-z0-9-~][a-z0-9-._~]*\/)?[a-z0-9-~][a-z0-9-._~]*$/
        const resolvedDependencies: string[] = []
        const dynamicClassName: string = `TempTranspile${this.app.Security.generateRandomString(16)}`
        const dynamicFunctionName: string = `__$TranspileFunction${this.app.Security.generateRandomString(16)}`
        const transpiledCodeForDetective: string = typescript.transpile(`class ${dynamicClassName}{[key:string]:any;async ${dynamicFunctionName}(){${sourceCode}}}`)
        const rawDependencies = detective(transpiledCodeForDetective)
        rawDependencies.forEach(rawDependency => {
            const parsedModuleInfo = moduleIdParser(rawDependency)
            const version: string = parsedModuleInfo?.version ? parsedModuleInfo.version : ''
            const dependency: string = (() => {
                if (moduleNameRegExp.test(rawDependency)) {
                    return rawDependency
                } else {
                    while (rawDependency && !moduleNameRegExp.test(rawDependency)) {
                        rawDependency = rawDependency.substring(0, rawDependency.length - 1)
                    }
                    if (rawDependency) {
                        return rawDependency
                    }
                }
                return ''
            })()
            if (dependency && !this.internalModules.includes(dependency)) {
                if (version) {
                    resolvedDependencies.push(`${dependency}@${version}`)
                } else {
                    resolvedDependencies.push(dependency)
                }
            }
        })
        return resolvedDependencies
    }

    /**
     * 执行编译
     */
    public async compile(sourceCodeObject: SourceCodeObject): Promise<CompiledCodeObject> {
        sourceCodeObject = Object.assign({
            environments: {
                type: 'object',
                properties: {}
            },
            schema: {
                type: 'object',
                properties: {}
            }
        }, sourceCodeObject)
        const nccCompileTmpDir: string = fs.mkdtempSync(path.resolve(this.tmpdir, `ncc-compile-${this.app.Security.generateRandomString(4).toLowerCase()}`))
        const compiled: Buffer = await this.nccCompile(nccCompileTmpDir, sourceCodeObject)
        const compiledCodeObject = {
            environments: sourceCodeObject.environments ? sourceCodeObject.environments : {type: 'object'},
            compiled: Array.from(compiled),
            schema: sourceCodeObject.schema
        }
        this.app.Shell.rm('-rf', nccCompileTmpDir)
        return compiledCodeObject
    }
}

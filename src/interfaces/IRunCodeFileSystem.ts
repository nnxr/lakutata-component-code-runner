export interface IRunCodeFileSystem {
    read: (runCodeId: string) => Promise<Buffer | undefined>
    write: (runCodeId: string, fileSystemBinary: Buffer) => Promise<void>
    sizeLimit: string | number
}
